export declare function readFile(path: string): string;
export declare function createObj<T>(): T;
export interface PTMap<T> {
    k: string;
    v: T;
}
export interface PMap<T> {
    k: number;
    v: T;
}
export declare function createPTMap<T>(): PTMap<T>;
export interface Weight {
    weight: number;
}
export interface Word extends Weight {
    word: string;
    offsets: number[];
}
export declare function createPMap<T>(): PMap<T>;
export declare const enum Comparison {
    LessThan = -1,
    EqualTo = 0,
    GreaterThan = 1,
}
export declare function weightCompare_V<T extends Weight>(a: T, b: T): Comparison;
export declare function weightCompare_A<T extends Weight>(a: T, b: T): Comparison;
