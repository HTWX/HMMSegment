"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mixSegment_1 = require("./mixSegment");
const utility_1 = require("./utility");
let segment_;
let idfMap_ = utility_1.createPTMap();
let idfAverage_ = 0;
let stopWords_ = [];
function createKeywordExtractor(dictPath, hmmFilePath, idfPath, stopWordPath, userDict = "") {
    if (!segment_) {
        segment_ = mixSegment_1.createMixSegment(dictPath, hmmFilePath, userDict);
        loadIdfDict(idfPath);
        loadStopWordDict(stopWordPath);
    }
    return {
        extr
    };
}
exports.createKeywordExtractor = createKeywordExtractor;
function extr(str, topN) {
    let words = segment_.cut(str);
    let wordmap = new Map();
    let offset = 0;
    for (let i = 0; i < words.length; ++i) {
        let t = offset;
        offset += words[i].length;
        if (stopWords_.indexOf(words[i]) !== -1) {
            continue;
        }
        let wr = utility_1.createObj();
        wr.offsets = [];
        wr.offsets.push(t);
        wr.weight = 1.0;
        wordmap.set(words[i], wr);
    }
    if (offset != str.length) {
        return;
    }
    let keys = [];
    wordmap.forEach((v, k) => {
        let cit = idfMap_[k];
        if (cit) {
            v.weight *= cit;
        }
        else {
            v.weight *= idfAverage_;
        }
        v.word = k;
        keys.push(v);
    });
    let keys2 = [];
    keys2 = keys.sort(utility_1.weightCompare_A);
    if (topN < keys2.length)
        keys2 = keys2.slice(0, topN);
    let ret = [];
    keys2.forEach(v => {
        console.log(v);
        ret.push(v.word);
    });
    return ret;
}
let content;
let line = "";
let i = 0;
function loadIdfDict(idfPath) {
    try {
        content = utility_1.readFile(idfPath).replace(/(\n\r|\n)/g, "\n").split("\n");
    }
    catch (e) {
        throw ("idfPath数据文件内容丢失");
    }
    if (!content) {
        throw ("idfPath数据文件内容丢失");
    }
    let buf = [];
    let idf = 0;
    let idfSum = 0;
    let lineno = 0;
    for (let i = 0; nextLine(); i++, lineno++) {
        if (!line) {
        }
        buf = line.split(" ");
        if (buf.length !== 2) {
        }
        idf = +buf[1];
        idfMap_[buf[0]] = idf;
        idfSum += idf;
    }
    if (!lineno) {
    }
    idfAverage_ = idfSum / lineno;
    if (idfAverage_ > 0) {
    }
}
function loadStopWordDict(filePath) {
    try {
        content = utility_1.readFile(filePath).replace(/(\n\r|\n)/g, "\n").split("\n");
    }
    catch (e) {
        throw ("idfPath数据文件内容丢失");
    }
    if (!content) {
        throw ("idfPath数据文件内容丢失");
    }
    while (nextLine()) {
        stopWords_.push(line);
    }
}
function nextLine() {
    function getLine() {
        line = content[i];
        i++;
        return line;
    }
    while (getLine()) {
        line = line.trim();
        if (!line) {
            continue;
        }
        if (line.indexOf("#") !== -1) {
            continue;
        }
        return true;
    }
    return false;
}
//# sourceMappingURL=keywordExtractor.js.map