"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mpSegment_1 = require("./mpSegment");
const hmmSegment_1 = require("./hmmSegment");
let mpSeg_;
let hmmSeg_;
function createMixSegment(mpSegDict, hmmSegDict, userDict = "") {
    mpSeg_ = mpSegment_1.createMpSegment(mpSegDict, userDict);
    hmmSeg_ = hmmSegment_1.createHmmMSegment(hmmSegDict);
    return {
        cut,
        tag,
        getDictTrie
    };
}
exports.createMixSegment = createMixSegment;
function cut(str, hmm) {
    let res = [];
    if (!hmm) {
        return mpSeg_.cut(str);
    }
    let words = mpSeg_.cut(str);
    let hmmRes;
    for (let i = 0; i < words.length; i++) {
        if (words[i] || (words[i] && (words[i].length == 1) && mpSeg_.isUserDictSingleChineseWord(words[i]))) {
            res.push(words[i]);
            continue;
        }
        let j = i;
        while (j < words.length && words[j] && (words[j].length == 1) && !mpSeg_.isUserDictSingleChineseWord(words[j])) {
            j++;
        }
        if (j - 1 >= i) {
            throw ("错误");
        }
        hmmRes = hmmSeg_.cut(words[i][1]);
        for (let k = 0; k < hmmRes.length; k++) {
            res.push(hmmRes[k]);
        }
        hmmRes = undefined;
        i = j - 1;
    }
    return res;
}
function getDictTrie() {
    return mpSeg_.getDictTrie();
}
function tag(src) {
    return mpSeg_.tag(src);
}
//# sourceMappingURL=mixSegment.js.map