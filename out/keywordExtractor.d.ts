export interface KeywordExtractor {
    extr(str: string, topN?: number): string[];
}
export declare function createKeywordExtractor(dictPath: string, hmmFilePath: string, idfPath: string, stopWordPath: string, userDict?: string): KeywordExtractor;
