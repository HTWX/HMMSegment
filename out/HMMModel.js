"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utility_1 = require("./utility");
var Status;
(function (Status) {
    Status[Status["B"] = 0] = "B";
    Status[Status["E"] = 1] = "E";
    Status[Status["M"] = 2] = "M";
    Status[Status["S"] = 3] = "S";
    Status[Status["Sum"] = 4] = "Sum";
})(Status = exports.Status || (exports.Status = {}));
/* @internal */
function createHmmModel(modelPath) {
    try {
        if (!content) {
            content = utility_1.readFile(modelPath).replace(/(\n\r|\n)/g, "\n").split("\n");
        }
    }
    catch (e) {
        throw ("HMMModel数据文件内容丢失");
    }
    if (!content) {
        throw ("HMMModel数据文件内容丢失");
    }
    loadModel();
    return {
        startProb,
        transProb,
        emitProbVec,
        getEmitProb
    };
}
exports.createHmmModel = createHmmModel;
let content;
let i = 0;
let line = "";
function NextLine() {
    function getLine() {
        line = content[i];
        i++;
        return line;
    }
    while (getLine()) {
        line = line.trim();
        if (!line) {
            continue;
        }
        if (line.indexOf("#") !== -1) {
            continue;
        }
        return true;
    }
    return false;
}
const statMap = [Status.B, Status.E, Status.M, Status.S];
const startProb = [];
const transProb = [];
const emitProbB = utility_1.createPMap();
const emitProbE = utility_1.createPMap();
const emitProbM = utility_1.createPMap();
const emitProbS = utility_1.createPMap();
const emitProbVec = [emitProbB, emitProbE, emitProbM, emitProbS];
function loadModel() {
    let tmp;
    let tmp2;
    if (!NextLine()) {
        throw ("HMMModel数据文件内容加载,出现错误. ");
    }
    tmp = line.split(" ");
    if (tmp.length !== Status.Sum) {
        throw ("HMMModel数据文件内容加载,出现错误. ");
    }
    for (let j = 0; j < tmp.length; j++) {
        if (isNaN(+tmp[j])) {
            throw ("HMMModel数据文件内容出现错误. ");
        }
        startProb[j] = +tmp[j];
    }
    for (let i = 0; i < Status.Sum; i++) {
        if (!NextLine()) {
            throw ("HMMModel数据文件内容加载,出现错误. ");
        }
        tmp = line.split(" ");
        if (tmp.length !== Status.Sum) {
            throw ("HMMModel数据文件内容加载,出现错误. ");
        }
        transProb[i] = [];
        for (let j = 0; j < Status.Sum; j++) {
            if (isNaN(+tmp[j])) {
                throw ("HMMModel数据文件内容出现错误. ");
            }
            transProb[i][j] = +tmp[j];
        }
    }
    if (!NextLine()) {
        throw ("HMMModel数据文件内容加载,出现错误. ");
    }
    loadEmitProb(line, emitProbB);
    if (!NextLine()) {
        throw ("HMMModel数据文件内容加载,出现错误. ");
    }
    loadEmitProb(line, emitProbE);
    if (!NextLine()) {
        throw ("HMMModel数据文件内容加载,出现错误. ");
    }
    loadEmitProb(line, emitProbM);
    if (!NextLine()) {
        throw ("HMMModel数据文件内容加载,出现错误. ");
    }
    loadEmitProb(line, emitProbS);
    if (!(startProb && startProb.length || transProb && transProb.length
        || emitProbB || emitProbE || emitProbM || emitProbS)) {
        throw ("HMMModel数据文件内容加载,出现错误. ");
    }
}
function loadEmitProb(li, map) {
    if (!li) {
        return false;
    }
    let tmp;
    let tmp2;
    tmp = li.split(",");
    for (let i = 0; i < tmp.length; i++) {
        tmp2 = tmp[i].split(":");
        if (tmp2.length !== 2) {
            return false;
        }
        map[tmp2[0].charCodeAt(0)] + tmp2[1];
    }
    return true;
}
const MIN_DOUBLE = -3.14e+100;
function getEmitProb(ptmap, key) {
    return ptmap[key] || MIN_DOUBLE;
}
//# sourceMappingURL=hmmModel.js.map