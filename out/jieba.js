"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mpSegment_1 = require("./mpSegment");
const mixSegment_1 = require("./mixSegment");
const hmmSegment_1 = require("./hmmSegment");
const dictTrie_1 = require("./dictTrie");
const keywordExtractor_1 = require("./keywordExtractor");
const pa = require("path");
const path1 = pa.resolve(__dirname, "../lib/jiebaDict.utf8");
const path2 = pa.resolve(__dirname, "../lib/jiebaHmmDict.utf8");
const path3 = pa.resolve(__dirname, "../lib/jiebaIdfDict.utf8");
const path4 = pa.resolve(__dirname, "../lib/jiebaUserDict.utf8");
const path5 = pa.resolve(__dirname, "../lib/stop_words.utf8");
function load(jiebaDictPath, jiebaHmmDictPath, jiebaIdfDictPath, jiebaUserDictPath) {
    jiebaDictPath = jiebaDictPath || path1;
    jiebaHmmDictPath = jiebaHmmDictPath || path2;
    jiebaIdfDictPath = jiebaIdfDictPath || path3;
    jiebaUserDictPath = jiebaUserDictPath || path4;
    const ke = keywordExtractor_1.createKeywordExtractor(jiebaDictPath, jiebaHmmDictPath, jiebaIdfDictPath, path5, jiebaUserDictPath);
    const tr = dictTrie_1.createDictTrie(jiebaDictPath, jiebaUserDictPath);
    const mp = mpSegment_1.createMpSegment(jiebaDictPath, jiebaUserDictPath);
    const mix = mixSegment_1.createMixSegment(jiebaDictPath, jiebaHmmDictPath);
    const hmm = hmmSegment_1.createHmmMSegment(jiebaHmmDictPath);
    return {
        cut: mp.cut,
        cutTag: mp.tag,
        cutHmm: hmm.cut,
        cutMix: mix.cut,
        cutMixTag: mix.tag,
        insertWord: tr.insertUserWord,
        extr: ke.extr
    };
}
exports.load = load;
exports.jieba = load();
//# sourceMappingURL=jieba.js.map