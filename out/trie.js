"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utility_1 = require("./utility");
exports.MAX_WORD_LENGTH = 512;
function createTrieNode() {
    return utility_1.createObj();
}
let root_;
/* @internal */
function createTrie(keys, valuePointers) {
    root_ = createTrieNode();
    if (!keys || !valuePointers) {
        return;
    }
    if (keys.length !== valuePointers.length) {
        return;
    }
    for (let i = 0; i < keys.length; i++) {
        insertNode(keys[i], valuePointers[i]);
    }
    return {
        getDictUnit,
        find,
        insertNode,
    };
}
exports.createTrie = createTrie;
function getDictUnit(word) {
    if (!word) {
        return;
    }
    let ptNode = root_;
    for (let i = 0; i < word.length; i++) {
        if (!ptNode.next) {
            return;
        }
        let it = ptNode.next[word[i]];
        if (!it) {
            return;
        }
        ptNode = it;
    }
    return ptNode.ptValue;
}
function find(str, max_word_len = exports.MAX_WORD_LENGTH) {
    if (!root_) {
        throw ("发生错误");
    }
    let res = [];
    let ptNode = null;
    for (let i = 0; i < str.length; i++) {
        if (!res[i]) {
            res[i] = utility_1.createObj();
        }
        res[i].runestr = str[i];
        let ls = root_.next[res[i].runestr];
        if (root_.next !== null && ls) {
            ptNode = ls;
        }
        else {
            ptNode = null;
        }
        let v = utility_1.createPMap();
        v.k = i;
        if (ptNode === null) {
            v.v = null;
            (res[i].nexts || (res[i].nexts = new Array())).push(v);
        }
        else {
            v.v = ptNode.ptValue;
            (res[i].nexts || (res[i].nexts = new Array())).push(v);
        }
        for (let j = i + 1; j < str.length && (j - i + 1) <= max_word_len; j++) {
            if (ptNode == null || ptNode.next == null) {
                break;
            }
            let ls = ptNode.next[str[j]];
            if (!ls) {
                break;
            }
            ptNode = ls;
            if (ptNode && ptNode.ptValue) {
                let v = utility_1.createPMap();
                v.k = j;
                v.v = ptNode.ptValue;
                res[i].nexts.push(v);
            }
        }
    }
    return res;
}
function insertNode(key, ptValue) {
    if (!key) {
        return;
    }
    let ptNode = root_;
    for (let i = 0, ls; i < key.length; i++) {
        ls = key[i];
        if (!ptNode.next) {
            ptNode.next = utility_1.createPTMap();
        }
        if (!ptNode.next[ls]) {
            let nextNode = createTrieNode();
            ptNode.next[ls] = nextNode;
            ptNode = nextNode;
        }
        else {
            ptNode = ptNode.next[ls];
        }
    }
    if (!ptNode) {
        throw ("发生错误");
    }
    ptNode.ptValue = ptValue;
}
//# sourceMappingURL=trie.js.map