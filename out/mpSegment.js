"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dictTrie_1 = require("./dictTrie");
const trie_1 = require("./trie");
const posTagger_1 = require("./posTagger");
let dictTrie_;
let isNeedDestroy_;
let tagger_;
function createMpSegment(dictPath, userDictPath = "") {
    if (!dictTrie_) {
        dictTrie_ = dictTrie_1.createDictTrie(dictPath, userDictPath);
    }
    if (!tagger_) {
        tagger_ = posTagger_1.createPosTagger();
    }
    return {
        cut,
        tag,
        getDictTrie,
        isUserDictSingleChineseWord,
    };
}
exports.createMpSegment = createMpSegment;
function cut(str, max_word_len) {
    let dags = dictTrie_.find(str, max_word_len || trie_1.MAX_WORD_LENGTH);
    CalcDp(dags);
    return cutByDag(str, dags);
}
function getDictTrie() {
    return dictTrie_;
}
function tag(str) {
    return tagger_.tag(str, { cut, getDictTrie });
}
function isUserDictSingleChineseWord(value) {
    return dictTrie_.isUserDictSingleChineseWord(value);
}
function CalcDp(dags) {
    let nextPos;
    let p;
    let val;
    for (let i = dags.length - 1; i >= 0; i--) {
        let rit = dags[i];
        rit.pInfo = null;
        rit.weight = dictTrie_1.MIN_DOUBLE;
        if (!rit.nexts) {
            throw ("抛出错误");
        }
        for (let ii = 0, it; ii < rit.nexts.length; ii++) {
            it = rit.nexts[ii];
            nextPos = it.k + 1;
            p = it.v;
            val = 0;
            if (nextPos < dags.length && dags[nextPos].weight) {
                val += dags[nextPos].weight;
            }
            if (p) {
                val += p.weight;
            }
            else {
                val += dictTrie_.getMinWeight();
            }
            if (val > rit.weight) {
                rit.pInfo = p;
                rit.weight = val;
            }
        }
    }
}
function cutByDag(begin, dags) {
    let i = 0;
    let words = [];
    while (i < dags.length) {
        let p = dags[i].pInfo;
        if (p && p.word) {
            let wr = begin.substring(i, i + p.word.length);
            words.push(wr);
            i += wr.length;
        }
        else {
            let wr = "";
            words.push(wr);
            i++;
        }
    }
    return words;
}
//# sourceMappingURL=mpSegment.js.map