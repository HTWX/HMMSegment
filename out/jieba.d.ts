export interface jieba {
    cut(str: string, maxWordLen?: number): string[];
    cutTag(str: string): string[];
    cutMix(str: string, hmm: boolean): string[];
    cutMixTag(str: string): string[];
    cutHmm(str: string): string[];
    insertWord(word: string, tag?: string): boolean;
    extr(str: string, topN?: number): string[];
}
export declare function load(jiebaDictPath?: string, jiebaHmmDictPath?: string, jiebaIdfDictPath?: string, jiebaUserDictPath?: string): {
    cut: (str: string, max_word_len?: number) => string[];
    cutTag: (str: string) => string[];
    cutHmm: (str: string) => string[];
    cutMix: (str: string, hmm?: boolean) => string[];
    cutMixTag: (src: string) => string[];
    insertWord: (word: string, tag?: string) => boolean;
    extr: (str: string, topN?: number) => string[];
};
export declare const jieba: jieba;
