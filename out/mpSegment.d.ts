import { DictTrie } from "./dictTrie";
import { Segment } from "./segment";
export interface MpSegment extends Segment {
    cut(str: string, max_word_len?: number): string[];
    tag(str: string): string[];
    getDictTrie(): DictTrie;
}
export declare function createMpSegment(dictPath: string, userDictPath?: string): {
    cut: (str: string, max_word_len?: number) => string[];
    tag: (str: string) => string[];
    getDictTrie: () => DictTrie;
    isUserDictSingleChineseWord: (value: string) => boolean;
};
