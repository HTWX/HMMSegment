export interface HmmSegment {
    cut(str: string): string[];
}
export declare function createHmmMSegment(hmmSegDict: string): HmmSegment;
export declare function cut(str: string): string[];
