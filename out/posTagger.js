"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const POS_M = "m";
const POS_ENG = "eng";
const POS_X = "x";
function createPosTagger() {
    return {
        tag
    };
}
exports.createPosTagger = createPosTagger;
function tag(str, seg) {
    let res = [];
    let CutRes = seg.cut(str);
    for (let i = 0, itr; i < CutRes.length; i++) {
        itr = CutRes[i];
        let v = `${itr}:${lookunTag(itr, seg)}`;
        res.push(v);
    }
    return res;
}
function lookunTag(itr, seg) {
    let tmp;
    let runs = itr;
    let dict = seg.getDictTrie();
    tmp = dict.getDictUnit(runs);
    if (!tmp) {
        return specialRule(runs);
    }
    else {
        return tmp.tag;
    }
}
function specialRule(ch) {
    let m = 0;
    let eng = 0;
    for (let i = 0; i < ch.length && eng < ch.length / 2; i++) {
        if (ch.charCodeAt(i) < 0x80) {
            eng++;
            if ('0'.charCodeAt(0) <= ch.charCodeAt(i) && ch.charCodeAt(i) <= '9'.charCodeAt(0)) {
                m++;
            }
        }
    }
    if (eng == 0) {
        return POS_X;
    }
    if (m == eng) {
        return POS_M;
    }
    return POS_ENG;
}
//# sourceMappingURL=posTagger.js.map