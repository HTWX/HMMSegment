import { Segment } from "./segment";
export interface PosTagger {
    tag(str: string, seg: Segment): string[];
}
export declare function createPosTagger(): PosTagger;
