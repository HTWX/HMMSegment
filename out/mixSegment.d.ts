import { DictTrie } from "./dictTrie";
import { Segment } from "./segment";
export interface MixSegment extends Segment {
    cut(str: string, hmm?: boolean): string[];
    tag(src: string): string[];
    getDictTrie(): DictTrie;
}
export declare function createMixSegment(mpSegDict: string, hmmSegDict: string, userDict?: string): MixSegment;
