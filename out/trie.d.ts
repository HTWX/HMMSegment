import { PMap, PTMap, Weight } from "./utility";
export declare const MAX_WORD_LENGTH = 512;
export interface DictUnit extends Weight {
    word: string;
    tag: string;
}
export interface Dag extends Weight {
    nexts: PMap<DictUnit>[];
    pInfo: DictUnit;
    runestr: string;
    nextPos: number;
}
export interface TrieNode {
    next: PTMap<TrieNode>;
    ptValue: DictUnit;
}
