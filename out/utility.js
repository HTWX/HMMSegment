"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
function readFile(path) {
    const buffer = fs.readFileSync(path);
    let len = buffer.length;
    if (len >= 2 && buffer[0] === 0xFE && buffer[1] === 0xFF) {
        len &= ~1;
        for (let i = 0; i < len; i += 2) {
            const temp = buffer[i];
            buffer[i] = buffer[i + 1];
            buffer[i + 1] = temp;
        }
        return buffer.toString("utf16le", 2);
    }
    if (len >= 2 && buffer[0] === 0xFF && buffer[1] === 0xFE) {
        return buffer.toString("utf16le", 2);
    }
    if (len >= 3 && buffer[0] === 0xEF && buffer[1] === 0xBB && buffer[2] === 0xBF) {
        return buffer.toString("utf8", 3);
    }
    return buffer.toString("utf8");
}
exports.readFile = readFile;
function createObj() {
    let v = Object.create(null);
    v.__ = null;
    delete v.__;
    return v;
}
exports.createObj = createObj;
function createPTMap() {
    return createObj();
}
exports.createPTMap = createPTMap;
function createPMap() {
    return createObj();
}
exports.createPMap = createPMap;
var Comparison;
(function (Comparison) {
    Comparison[Comparison["LessThan"] = -1] = "LessThan";
    Comparison[Comparison["EqualTo"] = 0] = "EqualTo";
    Comparison[Comparison["GreaterThan"] = 1] = "GreaterThan";
})(Comparison = exports.Comparison || (exports.Comparison = {}));
function weightCompare_V(a, b) {
    if (a === b)
        return 0 /* EqualTo */;
    if (a === undefined || a.weight === undefined)
        return 1 /* GreaterThan */;
    if (b === undefined || b.weight === undefined)
        return -1 /* LessThan */;
    if (a.weight === b.weight)
        return 0 /* EqualTo */;
    return a.weight > b.weight ? -1 /* LessThan */ : 1 /* GreaterThan */;
}
exports.weightCompare_V = weightCompare_V;
function weightCompare_A(a, b) {
    if (a === b)
        return 0 /* EqualTo */;
    if (a === undefined || a.weight === undefined)
        return -1 /* LessThan */;
    if (b === undefined || b.weight === undefined)
        return 1 /* GreaterThan */;
    if (a.weight === b.weight)
        return 0 /* EqualTo */;
    return a.weight > b.weight ? 1 /* GreaterThan */ : -1 /* LessThan */;
}
exports.weightCompare_A = weightCompare_A;
//# sourceMappingURL=utility.js.map