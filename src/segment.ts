import { DictTrie } from "./dictTrie";


export interface Segment {
    cut(str: string): string[]
    getDictTrie(): DictTrie

}