
import { createMpSegment } from "./mpSegment";
import { createMixSegment } from "./mixSegment";
import { createHmmMSegment } from "./hmmSegment";
import { createDictTrie } from "./dictTrie";
import { createKeywordExtractor } from "./keywordExtractor";
import * as pa from "path"

export interface jieba {

    cut(str: string, maxWordLen?: number): string[]
    cutTag(str: string): string[]
    cutMix(str: string, hmm: boolean): string[]
    cutMixTag(str: string): string[]
    cutHmm(str: string): string[]
    insertWord(word: string, tag ?:string):boolean
    extr(str: string, topN?: number): string[]

}

const path1 = pa.resolve(__dirname, "../lib/jiebaDict.utf8")
const path2 = pa.resolve(__dirname, "../lib/jiebaHmmDict.utf8")
const path3 = pa.resolve(__dirname, "../lib/jiebaIdfDict.utf8")
const path4 = pa.resolve(__dirname, "../lib/jiebaUserDict.utf8")
const path5 = pa.resolve(__dirname, "../lib/stop_words.utf8")

export function load(jiebaDictPath?: string, jiebaHmmDictPath?: string, jiebaIdfDictPath?: string, jiebaUserDictPath?: string) {
    jiebaDictPath = jiebaDictPath || path1
    jiebaHmmDictPath = jiebaHmmDictPath || path2
    jiebaIdfDictPath = jiebaIdfDictPath || path3
    jiebaUserDictPath = jiebaUserDictPath || path4
    const ke = createKeywordExtractor(jiebaDictPath, jiebaHmmDictPath, jiebaIdfDictPath, path5, jiebaUserDictPath)
    const tr = createDictTrie(jiebaDictPath, jiebaUserDictPath)
    const mp = createMpSegment(jiebaDictPath, jiebaUserDictPath)
    const mix = createMixSegment(jiebaDictPath, jiebaHmmDictPath)
    const hmm = createHmmMSegment(jiebaHmmDictPath)

    return {
        cut: mp.cut,
        cutTag: mp.tag,
        cutHmm: hmm.cut,
        cutMix: mix.cut,
        cutMixTag: mix.tag,
        insertWord:tr.insertUserWord,
        extr: ke.extr
    }

}

export const jieba: jieba = load()